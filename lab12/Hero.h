#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include "Templates.h"
#include "Stuff.h"

class Hero : public Character
{
protected:

    int _experience = 0;
    int _level = 0;

    std::string statName[6] = { "Strength", "Dexterity", "Endurance", "Inteligence", "Charisma", "Luck" };
    std::string _charName = "c_";

    void setStrength(int strength) { _strength = strength; }
    void setDexterity(int dexterity) { _dexterity = dexterity; }
    void setEndurance(int endurance) { _endurance = endurance; }
    void setInteligence(int inteligence) { _inteligence = inteligence; }
    void setCharisma(int charisma) { _charisma = charisma; }
    void setLuck(int luck) { _luck = luck; }
    void setName(std::string name) { _name = name; }
    void setProfession(std::string prof) { _profession = prof; }
    void setCharName(std::string newCharName) { _charName = newCharName; }
    void setExperience(int exp) { _experience = exp; }
    void setLevel(int lvl) { _level = lvl; }
    
    

    void addStrength(int strength) { _strength += strength; }
    void addDexterity(int dexterity) { _dexterity += dexterity; }
    void addEndurance(int endurance) { _endurance += endurance; }
    void addInteligence(int inteligence) { _inteligence += inteligence; }
    void addCharisma(int charisma) { _charisma += charisma; }
    void addLuck(int luck) { _luck += luck; }

    void addStrength() { _strength++; }
    void addDexterity() { _dexterity++; }
    void addEndurance() { _endurance++; }
    void addInteligence() { _inteligence++; }
    void addCharisma() { _charisma++; }
    void addLuck() { _luck++; }
    void addExperience() { _experience++; }
    void addLevel() { _level++; }




public:
    Hero() {}
    Hero(std::string name)
    {
        setName(name);
        load();
    }
    Hero(int statistics[], std::string name)
    {
        setStrength(statistics[0]);
        setDexterity(statistics[1]);
        setEndurance(statistics[2]);
        setInteligence(statistics[3]);
        setCharisma(statistics[4]);
        setLuck(statistics[5]);
        setName(name);
    }
    ~Hero()
    {
        save();
    }

    void LevelUp()
    {
        std::cout << "================ \nLevel Up! \n"\
            "================ \n";
        AllocatePoints(4);
        addLevel();
    }

    void AllocatePoints(int numberOfPoints)
    {

        do
        {
            std::cout << "You have " << numberOfPoints << " points available. \nPlease allocate them in the desired " \
                "statistic, by providing a statisctic ID. \n \n";


            displayStats();

            //std::cout << " \n \n";
            int w = 0;
            while (w < 6)
            {
                std::cout << "Use " << w + 1 << " for " << statName[w] << " \n";
                w++;
            }
            int idx;
            idx = Stuff::GetInt(1, 6);
            
            switch (idx)
            {
            case 1: {
                addStrength();
                break;
            }
            case 2: {
                addDexterity();
                break;
            }
            case 3: {
                addEndurance();
                break;
            }
            case 4: {
                addInteligence();
                break;
            }
            case 5: {
                addCharisma();
                break;
            }
            case 6: {
                addLuck();
                break;
            }
            }
            numberOfPoints--;
        } while (numberOfPoints > 0);

    }

    // Gets
    int GetStrength() { return _strength; }
    int GetDexterity() { return _dexterity; }
    int GetEndurance() { return _endurance; }
    int GetInteligence() { return _inteligence; }
    int GetCharisma() { return _charisma; }
    int GetLuck() { return _luck; }
    int GetExperience() { return _experience; }
    int GetLevel() { return _level; }
    int GetHP() { return _HP; }
    std::string GetName() { return _name; }
    std::string GetProfession() { return _profession; }

    //Moze byc public
    void setLevel() { _level = (GetExperience() / 10)+1; }
    void addExperience(int exp) { _experience += exp; }

    //walka
    void TakeDamage(int damage) { _HP -= damage; std::cout << GetName() << " has " << GetHP() << " HP left \n"; }
    bool IsDead(int exp) { if (_HP <= 0) { std::cout << GetName() << "is dead! \n"; addExperience(0 - (exp));return true; } else { return false; } }
    void SetHP() { _HP = (GetEndurance() * 10); }
    template <typename T>
    int calculateDMG(T& t)
    {
        if (GetStrength() >= GetInteligence() && GetStrength() >= GetDexterity())
        {
            return (GetStrength() * 3 - t.GetStrength());
        }
        else if (GetInteligence() >= GetStrength() && GetInteligence() >= GetDexterity())
        {
            return (GetInteligence() * 3 - t.GetInteligence());
        }
        else { return (GetDexterity() * 3 - t.GetDexterity()); }
    }

    template <typename T>
    void attack(T& t)
    {
        int dmg = calculateDMG(t);
        if (dmg <= 0) dmg = 1;
        std::cout << GetName() << " attacks for " << dmg << " \n";
        t.TakeDamage(dmg);
    }


    //zarzadzanie
    void save()
    {
        std::string FileName = _charName + GetName() + ".txt";
        std::fstream File;

        File.open(FileName, std::ios::out | std::ios::trunc);

        if (File.good())
        {
            File << "Hero prof \n" << GetProfession() << "\n";
            File << "Hero experience \n" << GetExperience() << "\n";
            File << "Hero strength \n" << GetStrength() << "\n";
            File << "Hero dexterity \n" << GetDexterity() << "\n";
            File << "Hero endurance \n" << GetEndurance() << "\n";
            File << "Hero inteligence \n" << GetInteligence() << "\n";
            File << "Hero charisma \n" << GetCharisma() << "\n";
            File << "Hero luck \n" << GetLuck() << "\n";
            File.close();
        }
        else std::cout << "Access denied \n";
    }
    void load()
    {
        std::string FileName = _charName + GetName() + ".txt";
        std::fstream File;
        File.open(FileName, std::ios::in);
        int statistic[7];
        if (File.good())
        {
            std::string fromFile;
            int tempVar = 1;
            while (std::getline(File, fromFile))
            {
                if (tempVar % 2 == 0) {
                    if (tempVar > 2)
                    {
                        int idx = (tempVar / 2) - 2;
                        statistic[idx] = std::stoi(fromFile);
                    }
                    else { setProfession(fromFile); }
                }
                tempVar++;
            }
            File.close();
        }
        setExperience(statistic[0]);
        setLevel();
        setStrength(statistic[1]);
        setDexterity(statistic[2]);
        setEndurance(statistic[3]);
        setInteligence(statistic[4]);
        setCharisma(statistic[5]);
        setLuck(statistic[6]);
    }
    void displayStats()
    {
        std::cout << GetName() << " statistics: \n";
        std::cout << "Hero profession:  " << GetProfession() << "\n";
        std::cout << "Hero experience:   " << GetExperience() << "\n";
        std::cout << "Hero level:   " << GetLevel() << "\n";
        std::cout << "Hero strength:  " << GetStrength() << "\n";
        std::cout << "Hero dexterity:  " << GetDexterity() << "\n";
        std::cout << "Hero endurance:  " << GetEndurance() << "\n";
        std::cout << "Hero inteligence:  " << GetInteligence() << "\n";
        std::cout << "Hero Charisma:  " << GetCharisma() << "\n";
        std::cout << "Hero Luck:  " << GetLuck() << "\n";

    }



    friend class Mage;
    friend class Warrior;
    friend class Berserker;
    friend class Thief;
    //friend class Monster;
};

class Mage
{
    int _initialInteligenceBoost = 4;
    std::string _profName = "mage";
    void assignProfession(Hero& h)
    {
        h.addInteligence(_initialInteligenceBoost);
        h.setProfession(_profName);

    }
public:
    Mage(Hero& h)
    {
        assignProfession(h);
    }
};

class Warrior
{
    int _initialEnduranceBoost = 4;
    std::string _profName = "warrior";
    void assignProfession(Hero& h)
    {
        h.addEndurance(_initialEnduranceBoost);
        h.setProfession(_profName);

    }
public:
    Warrior(Hero& h)
    {
        assignProfession(h);
    }
};

class Berserker
{
    int _initialStrengthBoost = 4;
    std::string _profName = "berserker";
    void assignProfession(Hero& h)
    {
        h.addStrength(_initialStrengthBoost);
        h.setProfession(_profName);

    }
public:
    Berserker(Hero& h)
    {
        assignProfession(h);
    }
};

class Thief
{
    int _initialDexterityBoost = 4;
    std::string _profName = "thief";
    void assignProfession(Hero& h)
    {
        h.addDexterity(_initialDexterityBoost);
        h.setProfession(_profName);

    }
public:
    Thief(Hero& h)
    {
        assignProfession(h);
    }
};