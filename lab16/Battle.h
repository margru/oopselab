#pragma once
#include <iostream>
#include <cmath>
#include "Hero.h"
#include "Monster.h"
#include <vector>

template <class R>
class Battle
{
	
public:
	Battle(Hero& t, R& r,std::vector<Item> eqItems)
	{
		std::cout << "\n \n### Battle has begun! ### \n";

		t.SetHP();
		r.SetHP();

		for (auto item : eqItems)
		{
			if (item.GetType() == 1)
			{
				std::cout << "Using " << item.GetName() << "\n";
				t.SetHP(item.GetDamage());
				std::cout << "Hero has additional " << item.GetDamage() << " HP! \n";
			}
			else if (item.GetType() == 2)
			{
				std::cout << "Using " << item.GetName() << "\n";
				r.SetHP(item.GetDamage()*(-1));
				std::cout << "Monster loses " << item.GetDamage() << " HP! \n";
			}
		}

		int exp = r.GetExperience();
		bool condition = true;
		while (condition) 
		{
			std::cout << t.GetName() << " has " << t.GetHP() << " \n";
			std::cout << r.GetName() << " has " << r.GetHP() << " \n";
			t.attack(r);
			r.attack(t);
			if (t.IsDead(exp)) { condition = false;}
			if (r.IsDead()) { condition = false; t.addExperience(exp); std::cout << "The hero has won! \nGained Exp: " << exp << " \n"; }
		}
		t.SetHP();
		r.SetHP();
		while (floor((t.GetExperience() / 10)) > t.GetLevel())
		{
			t.LevelUp();
		}
	}



};

