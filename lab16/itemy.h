#pragma once

#include<iostream>
#include<cstdio>
#include<cstdlib>




using namespace std;


class Item
{
	std::string name;
	int damage;
	int type;
	int drop;

public:
	std::string GetName();
	int GetDamage();
	int GetType();
	int GetDrop();
	
	Item(std::string name, int damage, int type, int drop) : damage(damage), name(name), type(type), drop(drop) {}
	Item() {}
};

struct node
{
	Item item;
	int quant = 1;
	struct node* next;
	struct node* prev;
};
//node *start;

///*
//Class Declaration
//*/
//
class double_llist
{
public:
	void create_list(Item item);
	void add_begin(Item item);
	void add_after(Item item, int position);
	void increase_quantity(int position);
	void delete_element(Item item);
	int search_element(Item item);
	void delete_element(int position);
	void display_dlist();
	int count();
	void reverse();
	node* at(int position);

	node* start;
	double_llist()
	{
		
		start = NULL;
	}
};

