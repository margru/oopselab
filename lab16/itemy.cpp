#include "itemy.h"

std::string Item::GetName() { return name; }
int Item::GetDamage() { return damage; }
int Item::GetType() { return type; }
int Item::GetDrop() { return drop; }

void double_llist::create_list(Item item)
{
	struct node* s, * temp;
	temp = new(struct node);
	temp->item = item;
	temp->next = NULL;
	if (start == NULL)
	{
		temp->prev = NULL;
		start = temp;
	}
	else
	{
		s = start;
		while (s->next != NULL)
			s = s->next;
		s->next = temp;
		temp->prev = s;
	}
}


/*
* Insertion at the beginning
*/
void double_llist::add_begin(Item item)
{
	if (start == NULL)
	{
		cout << "First Create the list." << endl;
		return;
	}
	struct node* temp;
	temp = new(struct node);
	temp->prev = NULL;
	temp->item = item;
	temp->next = start;
	start->prev = temp;
	start = temp;
	cout << "Element Inserted" << endl;
}

/*
* Insertion of element at a particular position
*/
void double_llist::add_after(Item item, int pos)
{
	if (start == NULL)
	{
		cout << "First Create the list." << endl;
		return;
	}
	struct node* tmp, * q;
	int i;
	q = start;
	for (i = 0;i < pos - 1;i++)
	{
		q = q->next;
		if (q == NULL)
		{
			cout << "There are less than ";
			cout << pos << " elements." << endl;
			return;
		}

	}
	tmp = new(struct node);
	tmp->item = item;
	if (q->next == NULL)
	{
		q->next = tmp;
		tmp->next = NULL;
		tmp->prev = q;
	}
	else
	{
		tmp->next = q->next;
		tmp->next->prev = tmp;
		q->next = tmp;
		tmp->prev = q;
	}
	cout << "Element Inserted" << endl;
}

/*
* Deletion of element from the list
*/
void double_llist::delete_element(Item item)
{
	struct node* tmp, * q;
	/*first element deletion*/
	if (start->item.GetName() == item.GetName())
	{
		tmp = start;
		start = start->next;
		start->prev = NULL;
		cout << "Element Deleted" << endl;
		free(tmp);
		return;
	}
	q = start;
	while (q->next->next != NULL)
	{
		/*Element deleted in between*/
		if (q->next->item.GetName() == item.GetName())
		{
			tmp = q->next;
			q->next = tmp->next;

			tmp->next->prev = q;
			cout << "Element Deleted" << endl;
			free(tmp);
			return;
		}
		q = q->next;
	}
	/*last element deleted*/
	if (q->next->item.GetName() == item.GetName())
	{
		tmp = q->next;
		free(tmp);
		q->next = NULL;
		cout << "Element Deleted" << endl;
		return;
	}
	cout << "Element " << item.GetName() << " not found" << endl;
}


// Deletion of element from the list using index
 
void double_llist::delete_element(int position)
{
	struct node* tmp, * q;
	int cnt = 0;
	/*first element deletion*/
	if ((position == 0) && start->next == NULL)
	{
		start = NULL;
		return;
	}

	if (position == 0)
	{
		tmp = start;
		start = start->next;
		start->prev = NULL;
		//cout << "Element Deleted" << endl;
		free(tmp);
		return;
	}
	q = start;
	cnt++;
	while (cnt < count())
	{
		/*Element deleted in between*/
		if (cnt == position)
		{
			tmp = q->next;
			q->next = tmp->next;
			if (tmp->next != nullptr)
			{
				tmp->next->prev = q;
			}
			//cout << "Element Deleted" << endl;
			free(tmp);
			return;
		}
		cnt++;
		q = q->next;
	}
	/*last element deleted*/
	if (position == count())
	{
		tmp = q->next;
		free(tmp);
		q->next = NULL;
		//cout << "Element Deleted" << endl;
		return;
	}
}

//  at
node* double_llist::at(int position)
{
	struct node* q;
	int index = 0;
	q = start;
	if (start == NULL)
	{
		cout << "List empty,nothing to return" << endl;
		return 0;
	}
	while (position > index)
	{
		q = q->next;
		index++;
	}
	return q;
}


/*
* Display elements of Doubly Link List
*/
void double_llist::display_dlist()
{
	struct node* q;
	int index = 0;

	if (start == NULL)
	{
		cout << "List empty,nothing to display" << endl;
		return;
	}
	q = start;

	while (q != NULL)
	{
		if (q->quant == 0)
		{
			delete_element(index);
			q = start;
			index = 0;
		}
		else
		{
			q = q->next;
			index++;
		}
	}

	index = 0;
	q = start;
	while (q != NULL)
	{
		if (q->quant == 0)
		{
			
		}
		else
		{
			cout << q->item.GetName() << " value: " << q->item.GetDamage() << " Quantity: " << q->quant << " ID: " << index + 1 << " \n";
			q = q->next;
			index++;
		}
	}
}

/*
* Number of elements in Doubly Link List
*/
int double_llist::count()
{
	struct node* q = start;
	int cnt = 0;
	while (q != NULL)
	{
		q = q->next;
		cnt++;
	}
	return cnt;
}

/*
	Increasing quantity 
*/
void double_llist::increase_quantity(int position)
{
	if (position <= count())
	{
		struct node* q = start;
		int count = 0;
		while (count != position)
		{
			count++;
			q = q->next;
		}
		q->quant++;
	}
	else 
	{
		std::cout << " Wrong position, index exceeds the range \n";
	}
}

/* searching
* returns index of a given element, if there is not any matching case, returns -1;
*/
int double_llist::search_element(Item item)
{
	//std::cout << "Searching if you already have " << item.GetName() << "\n";
	struct node* q = start;
	int count = 0;
	while (q != NULL)
	{
		//std::cout << " Element " << count << " with nane " << q->item.GetName() << "\n";
		if (q->item.GetName() == item.GetName()) return count;
		q = q->next;
		count++;
	}
	return -1;
}

/*
* Reverse Doubly Link List
*/
void double_llist::reverse()
{
	struct node* p1, * p2;
	p1 = start;
	p2 = p1->next;
	p1->next = NULL;
	p1->prev = p2;
	while (p2 != NULL)
	{
		p2->prev = p2->next;
		p2->next = p1;
		p1 = p2;
		p2 = p2->prev;
	}
	start = p1;
	cout << "List Reversed" << endl;
}
