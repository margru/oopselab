#pragma once
#include <string>
#include "Templates.h"
#include "Hero.h"
class Monster : public Hero
{
private:
    //std::string _familyName;
    
public:
    Monster(int statistics[], std::string name) : Hero(statistics, name) {};
    Monster(std::string name) : Hero(name) {}
    ~Monster()
    {}

    void displayStats()
    {
        std::cout << GetName() << " statistics: \n";
        std::cout << "Monster strength:  " << GetStrength() << "\n";
        std::cout << "Monster dexterity:  " << GetDexterity() << "\n";
        std::cout << "Monster endurance:  " << GetEndurance() << "\n";
        std::cout << "Monster inteligence:  " << GetInteligence() << "\n";
        std::cout << "Monster charisma:  " << GetCharisma() << "\n";
        std::cout << "Monster luck:  " << GetLuck() << "\n";

    }
    

};
