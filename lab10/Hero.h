#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include "Templates.h"

class Hero : public Character
{
protected:
    std::string _charName = "c_";
    void setStrength(int strength) { _strength = strength; }
    void setDexterity(int dexterity) { _dexterity = dexterity; }
    void setEndurance(int endurance) { _endurance = endurance; }
    void setInteligence(int inteligence) { _inteligence = inteligence; }
    void setCharisma(int charisma) { _charisma = charisma; }
    void setLuck(int luck) { _luck = luck; }
    void setName(std::string name) { _name = name; }
    void setProfession(std::string prof) { _profession = prof; }
    void setCharName(std::string newCharName) { _charName = newCharName; }

    void addStrength(int strength) { _strength += strength; }
    void addDexterity(int dexterity) { _dexterity += dexterity; }
    void addEndurance(int endurance) { _endurance += endurance; }
    void addInteligence(int inteligence) { _inteligence += inteligence; }
    void addCharisma(int charisma) { _charisma += charisma; }
    void addLuck(int luck) { _luck += luck; }



public:
    Hero() {}
    Hero(std::string name)
    {
        setName(name);
        load();
    }
    Hero(int statistics[], std::string name)
    {
        setStrength(statistics[0]);
        setDexterity(statistics[1]);
        setEndurance(statistics[2]);
        setInteligence(statistics[3]);
        setCharisma(statistics[4]);
        setLuck(statistics[5]);
        setName(name);
    }
    ~Hero()
    {
        save();
    }


    // Gets
    int GetStrength() { return _strength; }
    int GetDexterity() { return _dexterity; }
    int GetEndurance() { return _endurance; }
    int GetInteligence() { return _inteligence; }
    int GetCharisma() { return _charisma; }
    int GetLuck() { return _luck; }
    std::string GetName() { return _name; }
    std::string GetProfession() { return _profession; }


    void save()
    {
        std::string FileName = _charName + GetName() + ".txt";
        std::fstream File;

        File.open(FileName, std::ios::out | std::ios::trunc);

        if (File.good())
        {
            File << "Hero prof \n" << GetProfession() << "\n";
            File << "Hero strength \n" << GetStrength() << "\n";
            File << "Hero dexterity \n" << GetDexterity() << "\n";
            File << "Hero endurance \n" << GetEndurance() << "\n";
            File << "Hero inteligence \n" << GetInteligence() << "\n";
            File << "Hero charisma \n" << GetCharisma() << "\n";
            File << "Hero luck \n" << GetLuck() << "\n";
            File.close();
        }
        else std::cout << "Access denied \n";
    }

    void load()
    {
        std::string FileName = _charName + GetName() + ".txt";
        std::fstream File;
        File.open(FileName, std::ios::in);
        int statistic[6];
        if (File.good())
        {
            std::string fromFile;
            int tempVar = 1;
            while (std::getline(File, fromFile))
            {
                if (tempVar % 2 == 0) {
                    if (tempVar > 2)
                    {
                        int idx = (tempVar / 2) - 2;
                        statistic[idx] = std::stoi(fromFile);
                    }
                    else { setProfession(fromFile); }
                }
                tempVar++;
            }
            File.close();
        }
        setStrength(statistic[0]);
        setDexterity(statistic[1]);
        setEndurance(statistic[2]);
        setInteligence(statistic[3]);
        setCharisma(statistic[4]);
        setLuck(statistic[5]);
    }

    void displayStats()
    {
        std::cout << GetName() << " statistics: \n";
        std::cout << "Hero profession:  " << GetProfession() << "\n";
        std::cout << "Hero strength:  " << GetStrength() << "\n";
        std::cout << "Hero dexterity:  " << GetDexterity() << "\n";
        std::cout << "Hero endurance:  " << GetEndurance() << "\n";
        std::cout << "Hero inteligence:  " << GetInteligence() << "\n";
        std::cout << "Hero Charisma:  " << GetCharisma() << "\n";
        std::cout << "Hero Luck:  " << GetLuck() << "\n";

    }



    friend class Mage;
    friend class Warrior;
    friend class Berserker;
    friend class Thief;
    friend class Monster;
};

class Mage
{
    int _initialInteligenceBoost = 4;
    std::string _profName = "mage";
    void assignProfession(Hero& h)
    {
        h.addInteligence(_initialInteligenceBoost);
        h.setProfession(_profName);

    }
public:
    Mage(Hero& h)
    {
        assignProfession(h);
    }
};

class Warrior
{
    int _initialEnduranceBoost = 4;
    std::string _profName = "warrior";
    void assignProfession(Hero& h)
    {
        h.addEndurance(_initialEnduranceBoost);
        h.setProfession(_profName);

    }
public:
    Warrior(Hero& h)
    {
        assignProfession(h);
    }
};

class Berserker
{
    int _initialStrengthBoost = 4;
    std::string _profName = "berserker";
    void assignProfession(Hero& h)
    {
        h.addStrength(_initialStrengthBoost);
        h.setProfession(_profName);

    }
public:
    Berserker(Hero& h)
    {
        assignProfession(h);
    }
};

class Thief
{
    int _initialDexterityBoost = 4;
    std::string _profName = "thief";
    void assignProfession(Hero& h)
    {
        h.addDexterity(_initialDexterityBoost);
        h.setProfession(_profName);

    }
public:
    Thief(Hero& h)
    {
        assignProfession(h);
    }
};