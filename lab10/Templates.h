#pragma once
#include <iostream>
#include <string>
//#include "Hero.h"
class Character
{
protected:
    int _strength;
    int _dexterity;
    int _endurance;
    int _inteligence;
    int _charisma;
    int _luck;
    std::string _name;
    std::string _profession;
};

#include "Hero.h"


class Profession
{
    virtual void assignProfession() = 0;
};