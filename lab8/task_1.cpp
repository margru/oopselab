#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

class Hero
{
private:
    int _strength = 0;
    int _dexterity = 0;
    int _endurance = 0;
    int _inteligence = 0;
    int _charisma = 0;
    int _luck = 0;
    std::string _name;
    std::string _profession;


    // Sets
    void setStrength(int strength) { _strength += strength; }
    void setDexterity(int dexterity) { _dexterity += dexterity; }
    void setEndurance(int endurance) { _endurance += endurance; }
    void setInteligence(int inteligence) { _inteligence += inteligence; }
    void setCharisma(int charisma) { _charisma += charisma; }
    void setLuck(int luck) { _luck += luck; }
    void setName(std::string name) { _name = name; }
    void setProfession(std::string prof) { _profession = prof; }



public:
    Hero() {}
    Hero(std::string name)
    {
        setName(name);
        load();
    }
    Hero(int strength, int dexterity, int endurance, int inteligence, int charisma, int luck, std::string name)
    {
        setStrength(strength);
        setDexterity(dexterity);
        setEndurance(endurance);
        setInteligence(inteligence);
        setCharisma(charisma);
        setLuck(luck);
        setName(name);
    }
    ~Hero()
    {
        save();
    }


    // Gets
    int GetStrength() { return _strength; }
    int GetDexterity() { return _dexterity; }
    int GetEndurance() { return _endurance; }
    int GetInteligence() { return _inteligence; }
    int GetCharisma() { return _charisma; }
    int GetLuck() { return _luck; }
    std::string GetName() { return _name; }
    std::string GetProfession() { return _profession; }


    void save()
    {
        std::string FileName = GetName() + ".txt";
        std::fstream File;

        File.open(FileName, std::ios::out | std::ios::trunc);

        if (File.good())
        {
            File << "Hero prof \n" << GetProfession() << "\n";
            File << "Hero strength \n" << GetStrength() << "\n";
            File << "Hero dexterity \n" << GetDexterity() << "\n";
            File << "Hero endurance \n" << GetEndurance() << "\n";
            File << "Hero inteligence \n" << GetInteligence() << "\n";
            File << "Hero Charisma \n" << GetCharisma() << "\n";
            File << "Hero Luck \n" << GetLuck() << "\n";
            File.close();
        }
        else std::cout << "Access denied \n";
    }

    void load()
    {
        std::string FileName = GetName() + ".txt";
        std::fstream File;
        File.open(FileName, std::ios::in);
        int statistic[6];
        if (File.good())
        {
            std::string fromFile;
            int tempVar = 1;
            while (std::getline(File, fromFile))
            {
                if (tempVar % 2 == 0) {
                    if (tempVar > 2)
                    {
                        int idx = (tempVar / 2) - 2;
                        statistic[idx] = std::stoi(fromFile);
                    }
                    else { setProfession(fromFile); }
                }
                tempVar++;
            }
            File.close();
        }
        setStrength(statistic[0]);
        setDexterity(statistic[1]);
        setEndurance(statistic[2]);
        setInteligence(statistic[3]);
        setCharisma(statistic[4]);
        setLuck(statistic[5]);
    }

    void displayStats()
    {
        std::cout << GetName() << " statistics: \n";
        std::cout << "Hero profession:  " << GetProfession() << "\n";
        std::cout << "Hero strength:  " << GetStrength() << "\n";
        std::cout << "Hero dexterity:  " << GetDexterity() << "\n";
        std::cout << "Hero endurance:  " << GetEndurance() << "\n";
        std::cout << "Hero inteligence:  " << GetInteligence() << "\n";
        std::cout << "Hero Charisma:  " << GetCharisma() << "\n";
        std::cout << "Hero Luck:  " << GetLuck() << "\n";

    }



    friend class Mage;
    friend class Warrior;
    friend class Berserker;
    friend class Thief;
};

class Mage
{
    int _initialInteligenceBoost = 4;
    std::string _profName = "mage";
    void assignProfession(Hero& h)
    {
        h.setInteligence(_initialInteligenceBoost);
        h.setProfession(_profName);

    }
public:
    Mage(Hero& h)
    {
        assignProfession(h);
    }
};

class Warrior
{
    int _initialEnduranceBoost = 4;
    std::string _profName = "warrior";
    void assignProfession(Hero& h)
    {
        h.setEndurance(_initialEnduranceBoost);
        h.setProfession(_profName);

    }
public:
    Warrior(Hero& h)
    {
        assignProfession(h);
    }
};

class Berserker
{
    int _initialStrengthBoost = 4;
    std::string _profName = "berserker";
    void assignProfession(Hero& h)
    {
        h.setStrength(_initialStrengthBoost);
        h.setProfession(_profName);

    }
public:
    Berserker(Hero& h)
    {
        assignProfession(h);
    }
};

class Thief
{
    int _initialDexterityBoost = 4;
    std::string _profName = "thief";
    void assignProfession(Hero& h)
    {
        h.setDexterity(_initialDexterityBoost);
        h.setProfession(_profName);

    }
public:
    Thief(Hero& h)
    {
        assignProfession(h);
    }
};

class Engine
{
private:
    std::string _welcomeMessage = "The game has started! \n";

public:
    Engine()
    {
        std::cout << _welcomeMessage;
        bool condition = true;
        std::cout << "Insert 1 for loading a character, insert 2 for creating a new one \n";
        int userIn = GetInt();
        Hero hero;
        if (userIn == 0)
        {
            condition = false;
        }
        else if (userIn == 1)
        {
            std::string heroName;
            std::cout << "Please provide hero's name: \n";
            std::cin >> heroName;
            Hero p(heroName);
            hero = p;
            //p.displayStats();
        }
        else if (userIn == 2)
        {
            Hero p = createHero();
            hero = p;
            //p.displayStats();
        }
        //p.displayStats();
        while (condition)
        {
            std::cout << "Insert 1 for showing it's statistics, 0 for exiting the game \n";
            int userIn = GetInt();
            //Hero* p;
            if (userIn == 0)
            {
                condition = false;
                break;
            }
            else if (userIn == 1)
            {

                hero.displayStats();
            }
        }
    }

    //creating the Hero
    Hero createHero()
    {

        std::string HeroName;
        std::cout << "Name your character: \n";
        std::cin >> HeroName;
        std::cin.clear();
        int NumberOfPoints = 10;

        int statistics[6] = { 1, 1, 1, 1, 1, 1 };
        std::string statName[6] = { "Strength", "Dexterity", "Endurance", "Inteligence", "Charisma", "Luck" };

        do
        {
            std::cout << "You have " << NumberOfPoints << " points available. \nPlease allocate them in the desired " \
                "statistic, by providing a statisctic ID. \n \n";
            std::cout << "Your current stats are: \n";
            int i = 0;
            while (i < 6)
            {
                std::cout << statName[i] << ":  " << statistics[i] << " \n";
                i++;
            }
            std::cout << " \n \n";
            i = 0;
            while (i < 6)
            {
                std::cout << "Use " << i + 1 << " for " << statName[i] << " \n";
                i++;
            }
            int idx;
            while (true)
            {
                idx = GetInt();
                if (idx > 0 && idx < 7) { break; }
                else { std::cout << "Provide number between 1-6 \n"; }
            }
            idx--;
            statistics[idx]++;
            NumberOfPoints--;
        } while (NumberOfPoints > 0);

        std::cout << " Your current stats are: \n";
        int i = 0;
        while (i < 6)
        {
            std::cout << statName[i] << ":  " << statistics[i] << " \n";
            i++;
        }

        Hero herosik(statistics[0], statistics[1], statistics[2], statistics[3], statistics[4], statistics[5], HeroName);

        std::cout << "Choose your profession: \n \n";
        std::cout << "Use 1 for Mage \n";
        std::cout << "Use 2 for Warrior \n";
        std::cout << "Use 3 for Berserker \n";
        std::cout << "Use 4 for Thief \n";
        int input = GetInt(1, 4);

        if (input == 1)
        {
            Mage mag(herosik);
        }
        if (input == 2)
        {
            Warrior war(herosik);
        }
        if (input == 3)
        {
            Berserker ber(herosik);
        }
        if (input == 4)
        {
            Thief thi(herosik);
        }

        return herosik;
    }

    //Check input for int only, with range
    int GetInt(int a, int b)
    {
        int idx;
        while (true)
        {
            idx = GetInt();
            if (idx > a - 1 && idx < b + 1) { break; }
            else { std::cout << "Provide number between " << a << " - " << b << " \n"; }
        }
        return idx;
    }

    int GetInt()
    {
        int var;
        std::string userInput;
        int cond;
        cond = 1;
        // Check the corectness of user's input
        std::cin.clear();
        while (std::getline(std::cin, userInput))
        {
            std::stringstream ss(userInput);
            if (ss >> var)
            {
                if (ss.eof())
                {
                    //Success
                    cond = 0;
                    break;
                }
            }
            /*
            if (cond > 0)
            {
                std::cout.clear();
                std::cout << "Invalid input \n";
            }
            */

        }
        return var;
    }

};

int main()
{
    Engine engine;

    return 0;
}