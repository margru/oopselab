#pragma once
#include <iostream>
#include <cmath>
#include "Hero.h"
#include "Monster.h"

template <class R>
class Battle
{
	
public:
	Battle(Hero& t, R& r)
	{
		std::cout << "### Battle has begun! ### \n";
		t.SetHP();
		r.SetHP();
		int exp = r.GetExperience();
		bool condition = true;
		while (condition) 
		{
			std::cout << t.GetName() << " has " << t.GetHP() << " \n";
			std::cout << r.GetName() << " has " << r.GetHP() << " \n";
			t.attack(r);
			r.attack(t);
			if (t.IsDead(exp)) { condition = false;}
			if (r.IsDead()) { condition = false; t.addExperience(exp); std::cout << "The hero has won! \nGained Exp: " << exp << " \n"; }
		}
		t.SetHP();
		r.SetHP();
		while (floor((t.GetExperience() / 10)) > t.GetLevel())
		{
			t.LevelUp();
		}
	}



};

