﻿#include <sstream>
#include "Hero.h"
#include "Monster.h"
#include "Stuff.h"
#include "Battle.h"
//#include "itemy.h"

class Engine
{
private:
    std::string _welcomeMessage = "The game has started! \n";

public:
    Engine()
    {
        TsingleList sl;
        lst_el* p=nullptr;
        //int z;
        //int iteration = 1;

        std::cout << _welcomeMessage;
        bool condition = true;
        //Item item[3];
        //loadItems(item);
        std::cout << "Insert 1 for loading a character, insert 2 for creating a new one \n";
        int userIn = Stuff::GetInt();
        Hero hero;
        if (userIn == 0)
        {
            condition = false;
        }
        else if (userIn == 1)
        {
            std::string heroName;
            std::cout << "Please provide hero's name: \n";
            std::cin >> heroName;
            Hero p(heroName);
            hero = p;
            //p.displayStats();
        }
        else if (userIn == 2)
        {
            hero = createHero();

            //p.displayStats();
        }
        hero.displayStats();
        Monster monster[5];
        while (condition)
        {
            std::cout << "Insert 1 for creating a monster list, insert 2 for loading a monster list, 0 to continue \n";
            std::string names[5];
            int userIn = Stuff::GetInt();
            //Hero* p;
            if (userIn == 0)
            {
                condition = false;
                break;
            }
            else if (userIn == 1)
            {

                createMonster();
                
            }
            else if (userIn == 2)
            {

                loadMonsters(names);

                Monster m[5] = { Monster(names[0]), Monster(names[1]), Monster(names[2]) , Monster(names[3]) , Monster(names[4]) };
                
                for (int i = 0; i < 5; i++)
                {
                    m[i].displayStats();
                    m[i].setLevel();
                    monster[i] = m[i];
                    std::cout << "\n";
                }

            }
        }
        //hero.eq.create_list(1);
        condition = true;
        while (condition)
        {
            hero.SetHP();
            std::cout << "\n \n \nTo exit, use 0 \n";
            std::cout << "Please choose a monster to fight \n";

            for (int i = 0; i < 5; i++)
            {
                monster[i].displayStats();
                monster[i].SetHP();
                
                std::cout << "\nPlease use " << i+1 <<" for attaking this monster \n \n";
            }
            int in = Stuff::GetInt(0, 6);
            if (in == 0) { condition = false; break; }
            in--;

            Battle<Monster> battle(hero, monster[in]);
            /*if (monster[in].GetName().size() < 7)
            {
                hero.eq.add_after(item[(in % 2)+1].type, iteration);
                iteration++;
            }*/

          /*  std::cout << "Here Is your eq: \n";
            hero.eq.display_dlist();*/

            p = new lst_el;
            p->name = monster[in].GetName();
            sl.push_head(p);
            if (sl.size() > 10)
            {
                sl.rmTail();

            }
            cout << "Names: "; sl.showNames(); cout << endl;

            hero.save();
        }
    }

    /*void loadItems(Item items[])
    {
        Item item[3];
        std::fstream plik;
        plik.open("itemy.txt", std::ios::in);
        
        {
            int i = 0;
            std::string s;
            while (std::getline(plik, s))
            {
                if (i % 3 == 0)
                {
                    item[i / 3].name = s;
                }
                else if (i % 3 == 1)
                {
                    item[i / 3].damage = stoi(s);
                }
                else if (i % 3 == 2)
                {
                    item[i / 3].type = stoi(s);
                }
            }
        }
        items = item;
    }

    */    //creating the Hero
        Hero createHero()
        {

            std::string HeroName;
            std::cout << "Name your character: \n";
            std::cin >> HeroName;
            std::cin.clear();
            int NumberOfPoints = 10;

            int statistics[6] = { 1, 1, 1, 1, 1, 1 };
            std::string statName[6] = { "Strength", "Dexterity", "Endurance", "Inteligence", "Charisma", "Luck" };

            do
            {
                std::cout << "You have " << NumberOfPoints << " points available. \nPlease allocate them in the desired " \
                    "statistic, by providing a statisctic ID. \n \n";
                std::cout << "Your current stats are: \n";
                int w = 0;
                while (w < 6)
                {
                    std::cout << statName[w] << ":  " << statistics[w] << " \n";
                    w++;
                }
                //std::cout << " \n \n";
                w = 0;
                while (w < 6)
                {
                    std::cout << "Use " << w + 1 << " for " << statName[w] << " \n";
                    w++;
                }
                int idx;
                idx = Stuff::GetInt(1, 6);
                idx--;
                statistics[idx]++;
                NumberOfPoints--;
            } while (NumberOfPoints > 0);

            std::cout << " Your current stats are: \n";
            int i = 0;
            while (i < 6)
            {
                std::cout << statName[i] << ":  " << statistics[i] << " \n";
                i++;
            }

            Hero herosik(statistics, HeroName);
            herosik.displayStats();
            std::cout << "Choose your profession: \n \n";
            std::cout << "Use 1 for Mage \n";
            std::cout << "Use 2 for Warrior \n";
            std::cout << "Use 3 for Berserker \n";
            std::cout << "Use 4 for Thief \n";
            int input = Stuff::GetInt(1, 4);

            if (input == 1)
            {
                Mage mag(herosik);
            }
            if (input == 2)
            {
                Warrior war(herosik);
            }
            if (input == 3)
            {
                Berserker ber(herosik);
            }
            if (input == 4)
            {
                Thief thi(herosik);
            }

            return herosik;
        }

        void createMonster()
        {
            int monsterStat[5][7] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            for (int j = 0;j < 5;j++)
            {
                for (int i = 0; i < 6; i++)
                {
                    monsterStat[j][i] = Stuff::GetRandom(1, 10);
                }
            }
            std::string monsterNames[5];
            for (int i = 0; i < 5;i++) {
                monsterNames[i] = Stuff::GetRandom();
            }
            //debugg(monsterStat[0]);

            Monster monster[5] = { Monster(monsterStat[0],monsterNames[0]), Monster(monsterStat[1], monsterNames[1]), Monster(monsterStat[2], monsterNames[2]) ,\
                Monster(monsterStat[3], monsterNames[3]) , Monster(monsterStat[4], monsterNames[4]) };
            /*
            std::vector<Monster> v;
            for (int i = 0; i < 5; i++)
            {
                Monster
                v.push_back()
            }
            */

            for (int i = 0; i < 5; i++)
            {
                monster[i].displayStats();
                std::cout << "\n";
            }
            std::cout << "Do you want to save the current monster list? \n";
            std::cout << "Y/N \n";
            std::string in;
            getline(std::cin, in);
            if (in == "Y" || in == "y")
            {
                std::string names[5];
                for (int i = 0; i < 5; i++)
                {
                    names[i] = monster[i].GetName();
                    monster[i].save();
                }
                saveMonsters(names);
                std::cout << "Monsters saved correctly. \n";
            }
        }
        void saveMonsters(std::string names[])
        {
            std::string name;
            std::cout << "Provide name for list of monsters: ";
            getline(std::cin, name);
            std::string FileName = name + ".txt";
            std::fstream File;

            File.open(FileName, std::ios::out | std::ios::trunc);

            if (File.good())
            {
                for (int i = 0; i < 5; i++)
                {
                    File << names[i] << "\n";

                }
                File.close();
            }
            else std::cout << "Access denied \n";
        }
        void loadMonsters(std::string names[])
        {
            std::string name;
            std::cout << "Provide name for list of monsters: ";
            getline(std::cin, name);
            std::string FileName = name + ".txt";
            std::fstream File;

            File.open(FileName, std::ios::in);

            if (File.good())
            {
                std::string fromFile;
                int idx = 0;

                while (std::getline(File, fromFile))
                {
                    names[idx] = fromFile;
                    idx++;
                }
                File.close();
            }
            else std::cout << "Access denied \n";
        }



    };

    int main()
    {
        Engine engine;

        return 0;
    }
