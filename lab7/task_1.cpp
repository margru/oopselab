#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

class Hero
{
private:
        int _strength;
        int _dexterity;
        int _endurance;
        int _inteligence;
        int _charisma;
        int _luck;
        std::string _name;

        // Sets
        void setStrength(int strength) {_strength = strength; }
        void setDexterity(int dexterity) { _dexterity = dexterity; }
        void setEndurance(int endurance) { _endurance = endurance; }
        void setInteligence(int inteligence) { _inteligence = inteligence; }
        void setCharisma(int charisma) { _charisma = charisma; }
        void setLuck(int luck) { _luck = luck; }
        void setName(std::string name) { _name = name; }



public:
    Hero(){}
    Hero(std::string name)
    {
        setName(name);
        load();
    }
    Hero(int strength, int dexterity, int endurance, int inteligence, int charisma, int luck, std::string name)
    {
        setStrength(strength);
        setDexterity(dexterity);
        setEndurance(endurance);
        setInteligence(inteligence);
        setCharisma(charisma);
        setLuck(luck);
        setName(name);
    }
    ~Hero()
    {
        save();
    }


    // Gets
    int GetStrength() { return _strength; }
    int GetDexterity() { return _dexterity; }
    int GetEndurance() { return _endurance; }
    int GetInteligence() { return _inteligence; }
    int GetCharisma() { return _charisma; }
    int GetLuck() { return _luck; }
    std::string GetName() { return _name; }


    void save()
    {
        std::string FileName = GetName() + ".txt";
        std::fstream File;

        File.open(FileName, std::ios::out | std::ios::trunc );

        if (File.good())
        {
            File << "Hero strength \n" << GetStrength() << "\n";
            File << "Hero dexterity \n" << GetDexterity() << "\n";
            File << "Hero endurance \n" << GetEndurance() << "\n";
            File << "Hero inteligence \n" << GetInteligence() << "\n";
            File << "Hero Charisma \n" << GetCharisma() << "\n";
            File << "Hero Luck \n" << GetLuck() << "\n";
            File.close();
        }
        else std::cout << "Access denied \n";
    }

    void load()
    {
        std::string FileName = GetName() + ".txt";
        std::fstream File;
        File.open(FileName, std::ios::in);
        int statistic[6];
        if (File.good())
        {
            std::string fromFile;
            int tempVar = 1;
            while (std::getline(File, fromFile))
            {
                if (tempVar % 2 == 0)
                {
                    int idx = (tempVar / 2) - 1;
                    statistic[idx] = std::stoi(fromFile);
                }
                tempVar++;
            }
            File.close();
        }
        setStrength(statistic[0]);
        setDexterity(statistic[1]);
        setEndurance(statistic[2]);
        setInteligence(statistic[3]);
        setCharisma(statistic[4]);
        setLuck(statistic[5]);
    }
    
    void displayStats()
    {
        std::cout << GetName() << " statistics: \n";
        std::cout << "Hero strength:  " << GetStrength() << "\n";
        std::cout << "Hero dexterity:  " << GetDexterity() << "\n";
        std::cout << "Hero endurance:  " << GetEndurance() << "\n";
        std::cout << "Hero inteligence:  " << GetInteligence() << "\n";
        std::cout << "Hero Charisma:  " << GetCharisma() << "\n";
        std::cout << "Hero Luck:  " << GetLuck() << "\n";

    }



    // friend class Engine;
};

class Engine
{
private:
    std::string _welcomeMessage = "The game has started! \n";


public:
    Engine()
    {
        std::cout << _welcomeMessage;
        bool condition = true;
        std::cout << "Insert 1 for loading a character, insert 2 for creating a new one \n";
        int userIn = GetInt();
        Hero hero;
        if (userIn == 0)
        {
            condition = false;
        }
        else if (userIn == 1)
        {
            std::string heroName;
            std::cout << "Please provide hero's name: \n";
            std::cin >> heroName;
            Hero p(heroName);
            hero = p;
            //p.displayStats();
        }
        else if (userIn == 2)
        {
            Hero p = createHero();
            hero = p;
            //p.displayStats();
        }
        //p.displayStats();
        while (condition)
        {
            std::cout << "Insert 1 for showing it's statistics, 0 for exiting the game \n";
            int userIn = GetInt();
            //Hero* p;
            if (userIn == 0)
            {
                condition = false;
                break;
            }
            else if (userIn == 1)
            {

               hero.displayStats();
            }
        }
    }


   

    //creating the Hero
    Hero createHero()
    {

        std::string HeroName;
        std::cout << "Name your character: \n";
        std::cin >> HeroName;
        std::cin.clear();
        int NumberOfPoints = 10;

        int statistics[6] = { 1, 1, 1, 1, 1, 1 };
            std::string statName[6] = {"Strength", "Dexterity", "Endurance", "Inteligence", "Charisma", "Luck"};

        do
        {
            std::cout << "You have " << NumberOfPoints << " points available. \nPlease allocate them in the desired " \
                "statistic, by providing a statisctic ID. \n \n";
            std::cout << "Your current stats are: \n";
            int i = 0;
            while (i < 6)
            {
                 std::cout << statName[i] << ":  " << statistics[i] << " \n";
                i++;
            }
            std::cout << " \n \n";
            i = 0;
            while (i < 6)
            {
                std::cout << "Use " << i + 1 << " for " << statName[i] << " \n";
                i++;
            }
            int idx;
            while (true) 
            {
             idx   = GetInt();
             if (idx > 0 && idx < 7) { break; }
             else { std::cout << "Provide number between 1-6 \n"; }
            }
                    idx--;
                    statistics[idx]++;
                    NumberOfPoints--;
        } while (NumberOfPoints > 0);

        std::cout << " Your current stats are: \n";
        int i = 0;
        while (i < 6)
        {
            std::cout << statName[i] << ":  " << statistics[i] << " \n";
            i++;
        }

        Hero herosik(statistics[0], statistics[1], statistics[2], statistics[3], statistics[4], statistics[5], HeroName);
        
        return herosik;
    }



    //Check input for 
    int GetInt()
    {
        int var;
        std::string userInput;
        int cond;
        cond = 1;
        // Check the corectness of user's input
        std::cin.clear();
        while (std::getline(std::cin, userInput))
        {
            std::stringstream ss(userInput);
            if (ss >> var)
            {
                if (ss.eof())
                {
                        //Success
                    cond = 0;
                        break;
                }
            }
            /*
            if (cond > 0)
            {
                std::cout.clear();
                std::cout << "Invalid input \n";
            }
            */
            
        }
        return var;
    }

};

int main()
{
    Engine engine;

    return 0;
}