#pragma once
#include <cmath>
#include <string>
#include "Templates.h"
#include "Hero.h"
class Monster :
     public Hero
{
private:
    int _availableexp;
    //void setExperience(int exp) { _availableexp = exp; }
    

public:
    Monster(int statistics[], std::string name) : Hero(statistics, name) {};
    Monster(std::string name) : Hero(name) {};
    Monster() {};
    ~Monster()
    {}
    //int GetExperience() { return _availableexp; }
    void displayStats()
    {
        setLevel();
        std::cout << GetName() << " statistics: \n";
        std::cout << "Monster level:  " << GetLevel() << "\n";
        std::cout << "Monster strength:  " << GetStrength() << "\n";
        std::cout << "Monster dexterity:  " << GetDexterity() << "\n";
        std::cout << "Monster endurance:  " << GetEndurance() << "\n";
        std::cout << "Monster inteligence:  " << GetInteligence() << "\n";
        std::cout << "Monster charisma:  " << GetCharisma() << "\n";
        std::cout << "Monster luck:  " << GetLuck() << "\n";

    }
    void setLevel()  {  int totalStat = GetStrength() + GetDexterity() + GetInteligence() + GetEndurance() + GetLuck();
    int vat = ceil(((totalStat - 16) / 4))+ 1;
    Hero::setLevel(vat);
    setExperience(_level * 5);
    }
    bool IsDead() { if (_HP <= 0) { std::cout << GetName() << " is dead! \n"; return true; } else { return false; } }

    

};
