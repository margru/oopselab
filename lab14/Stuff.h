#pragma once
#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>
#include <sstream>
class Stuff
{
public:
    //Useless
    static void debugg(int t[])
    {
        int i = 0;
        while (i < 7)
        {
            std::cout << t[i] << "\n";
            i++;
        }

    }

    //Get random number within given interval
    static int GetRandom(int min, int max)
    {
        static bool FirstTime = true;
        if (FirstTime)
        {
            srand(time(NULL));
            FirstTime = false;
            /*
std::random_device rd; // random number form the hardware
std::mt19937 gen(rd()); //Marsenne Twister pseudo-random generator
std::uniform_int_distribution<> distr(0, 10);
*/
        }

        return (min + rand() % ((max + 1) - min));
    }

    //Get random name with a random length
    static std::string GetRandom()
    {
        const char consonents[19] = { 'w', 'r', 't', 'p', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm' };
        const char vowels[6] = { 'a', 'e', 'y', 'u', 'i', 'o' };
        std::string name = "";
        int count = 0;
        int length = GetRandom(3, 15);
        int random = 0;
        while (length > 0)
        {
            if (random < 2 && count < 1)
            {
                name += consonents[GetRandom(0, 18)];
                count++;
            }
            else
            {
                name += vowels[GetRandom(0, 5)];
                count = 0;
            }
            random = GetRandom(0, 2);
            length--;
        }
        return name;
    }

    //Check input for int only, within a given range
    static int GetInt(int a, int b)
    {
        int idx;
        while (true)
        {
            idx = GetInt();
            if (idx > a - 1 && idx < b + 1) { break; }
            else { std::cout << "Provide number between " << a << " - " << b << " \n"; }
        }
        return idx;
    }

    //Check input for int only
    static int GetInt()
    {
        int var;
        std::string userInput;
        int cond;
        cond = 1;
        // Check the corectness of user's input
        std::cin.clear();
        while (std::getline(std::cin, userInput))
        {
            std::stringstream ss(userInput);
            if (ss >> var)
            {
                if (ss.eof())
                {
                    //Success
                    cond = 0;
                    break;
                }
            }
            /*
            if (cond > 0)
            {
                std::cout.clear();
                std::cout << "Invalid input \n";
            }
            */

        }
        return var;
    }

};