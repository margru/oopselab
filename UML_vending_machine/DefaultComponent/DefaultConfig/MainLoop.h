/*********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: MainLoop
//!	Generated Date	: Sat, 11, Jun 2022  
	File Path	: DefaultComponent/DefaultConfig/MainLoop.h
*********************************************************************/

#ifndef MainLoop_H
#define MainLoop_H

//## auto_generated
#include <oxf/oxf.h>
//## link itsCoinDist
#include "CoinDist.h"
//## link itsProductDist_1
#include "ProductDist.h"
//## package Vendigo

//## class MainLoop
class MainLoop {
    ////    Constructors and destructors    ////
    
public :

    //## operation MainLoop()
    MainLoop();
    
    //## auto_generated
    ~MainLoop();
    
    ////    Operations    ////
    
    //## operation run()
    void run();
    
    ////    Additional operations    ////
    
    //## auto_generated
    CoinDist* getItsCoinDist() const;
    
    //## auto_generated
    ProductDist* getItsProductDist_1() const;

protected :

    //## auto_generated
    void initRelations();
    
    ////    Relations and components    ////
    
    CoinDist itsCoinDist;		//## link itsCoinDist
    
    ProductDist itsProductDist_1;		//## link itsProductDist_1
};

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/MainLoop.h
*********************************************************************/
