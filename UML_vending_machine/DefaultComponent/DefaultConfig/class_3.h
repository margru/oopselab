/*********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: class_3
//!	Generated Date	: Sat, 11, Jun 2022  
	File Path	: DefaultComponent/DefaultConfig/class_3.h
*********************************************************************/

#ifndef class_3_H
#define class_3_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include "Vendigo.h"
//## package Vendigo

//## class class_3
class class_3 {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    class_3();
    
    //## auto_generated
    ~class_3();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/class_3.h
*********************************************************************/
