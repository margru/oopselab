/********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: CoinDist
//!	Generated Date	: Sat, 11, Jun 2022  
	File Path	: DefaultComponent/DefaultConfig/CoinDist.cpp
*********************************************************************/

//## auto_generated
#include "CoinDist.h"
//## link itsMainLoop
#include "MainLoop.h"
//## package Vendigo

//## class CoinDist
CoinDist::CoinDist() : groszy10(4), groszy20(20), groszy50(10), zloty1(0), zloty2(2), zloty5(0) {
    itsMainLoop = NULL;
    //#[ operation CoinDist()
    std::cout << "Coin dist is here too! \n";
    //#]
}

CoinDist::~CoinDist() {
    cleanUpRelations();
}

void CoinDist::DisplayCoins() {
    //#[ operation DisplayCoins()
    std::cout << "Coins: \n";
    std::cout << Get10() << "\n";
    std::cout << Get20() << "\n";
    std::cout << Get50() << "\n";
    std::cout << Get100() << "\n";
    std::cout << Get200() << "\n";
    std::cout << Get500() << "\n";
    //#]
}

int CoinDist::Get10() {
    //#[ operation Get10()
    return groszy10;
    //#]
}

int CoinDist::Get100() {
    //#[ operation Get100()
    return zloty1;
    //#]
}

int CoinDist::Get20() {
    //#[ operation Get20()
    return groszy20;
    //#]
}

int CoinDist::Get200() {
    //#[ operation Get200()
    return zloty2;
    //#]
}

int CoinDist::Get50() {
    //#[ operation Get50()
    return groszy50;
    //#]
}

int CoinDist::Get500() {
    //#[ operation Get500()
    return zloty5;
    //#]
}

bool CoinDist::PayForProduct(float pricee) {
    //#[ operation PayForProduct(float)
    double paid = 0;
    double price = pricee;
    int temp010 = 0;
    int temp020 = 0;
    int temp050 = 0;
    int temp100 = 0;
    int temp200 = 0;
    int temp500 = 0;
    while(paid<price)
    {
    	double coin;
    	std::cout<< " You have to pay " << price - paid;
    	std::cout << "\nIf you have not enough coins, press 0 \n";
    	std::cin >> coin;
    	if(coin == 0) break;
    	if(coin == 0.10) { temp010++; paid+= 0.10; }
    	else if(coin == 0.20) { temp020++; paid+= 0.20; }
    	else if(coin == 0.50) { temp050++; paid+= 0.50; }
    	else if(coin == 1) { temp100++; paid+= 1; }
    	else if(coin == 2) { temp200++; paid+= 2; }
    	else if(coin == 5) { temp500++; paid+= 5; }
    	std::cout<< "You have paid " << paid <<std::endl;
    }
    if(paid < price) 
    {
    	std::cout << "Giving back the money \n";
    	return false;
    }
    else 
    {
    	// giving the rest
    	double rest = paid - price;
    	std::cout<< "Rest: " <<rest <<"\n";
    	
    	int ttemp010 = 0;
    	int ttemp020 = 0;
    	int ttemp050 = 0;
    	int ttemp100 = 0;
    	int ttemp200 = 0;
    	
    	while(rest > 0.01)
    	{
    	std::cout<< rest <<"\n";
    		if(rest >= 2 && (ttemp200 < zloty2 + temp200)&& !(zloty2 + temp200 == 0)) { ttemp200++; rest-=2; }
    		else if(rest >= 1 && (ttemp100 < zloty1 + temp100) && !(zloty1 + temp100 == 0)) { ttemp100++; rest-=1; }
    		else if(rest >= 0.5 && (ttemp050 < groszy50 + temp050)&& !(groszy50 + temp050 == 0)) { ttemp050++; rest-=0.5; }
    		else if(rest >= 0.2 && (ttemp020 < groszy20 + temp020)&& !(groszy20 + temp020 == 0)) { ttemp020++; rest-=0.2; }
    		else if(rest >= 0.1 && (ttemp010 < groszy10 + temp010)&& !(groszy10 + temp010 == 0)) { ttemp010++; rest-=0.1; }
    		else {std::cout<< "We cannot give the rest back"; return false;}
    		
    	}
    	
    	groszy10 += temp010;
    	groszy20 += temp020;
    	groszy50 += temp050;
    	zloty1 += temp100;
    	zloty2 += temp200;
    	zloty5 += temp500;
    	
    	groszy10 -= ttemp010;
    	groszy20 -= ttemp020;
    	groszy50 -= ttemp050;
    	zloty1 -= ttemp100;
    	zloty2 -= ttemp200;
    	
    	return true;
    	
    }
    //#]
}

int CoinDist::getGroszy10() const {
    return groszy10;
}

void CoinDist::setGroszy10(int p_groszy10) {
    groszy10 = p_groszy10;
}

int CoinDist::getGroszy20() const {
    return groszy20;
}

void CoinDist::setGroszy20(int p_groszy20) {
    groszy20 = p_groszy20;
}

int CoinDist::getGroszy50() const {
    return groszy50;
}

void CoinDist::setGroszy50(int p_groszy50) {
    groszy50 = p_groszy50;
}

int CoinDist::getZloty1() const {
    return zloty1;
}

void CoinDist::setZloty1(int p_zloty1) {
    zloty1 = p_zloty1;
}

MainLoop* CoinDist::getItsMainLoop() const {
    return itsMainLoop;
}

void CoinDist::setItsMainLoop(MainLoop* p_MainLoop) {
    _setItsMainLoop(p_MainLoop);
}

void CoinDist::cleanUpRelations() {
    if(itsMainLoop != NULL)
        {
            itsMainLoop = NULL;
        }
}

int CoinDist::getZloty2() const {
    return zloty2;
}

void CoinDist::setZloty2(int p_zloty2) {
    zloty2 = p_zloty2;
}

int CoinDist::getZloty5() const {
    return zloty5;
}

void CoinDist::setZloty5(int p_zloty5) {
    zloty5 = p_zloty5;
}

void CoinDist::__setItsMainLoop(MainLoop* p_MainLoop) {
    itsMainLoop = p_MainLoop;
}

void CoinDist::_setItsMainLoop(MainLoop* p_MainLoop) {
    __setItsMainLoop(p_MainLoop);
}

void CoinDist::_clearItsMainLoop() {
    itsMainLoop = NULL;
}

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/CoinDist.cpp
*********************************************************************/
