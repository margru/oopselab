/********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: ProductDistribution
//!	Generated Date	: Sat, 11, Jun 2022  
	File Path	: DefaultComponent/DefaultConfig/ProductDistribution.cpp
*********************************************************************/

//## auto_generated
#include "ProductDistribution.h"
//## link itsMainLoop
#include "MainLoop.h"
//## package Vendigo

//## class ProductDistribution
ProductDistribution::ProductDistribution() {
    itsMainLoop = NULL;
    //#[ operation ProductDistribution()
    std::cout << "Product dis. is running! \n";
    //#]
}

ProductDistribution::~ProductDistribution() {
    cleanUpRelations();
}

MainLoop* ProductDistribution::getItsMainLoop() const {
    return itsMainLoop;
}

void ProductDistribution::setItsMainLoop(MainLoop* p_MainLoop) {
    if(p_MainLoop != NULL)
        {
            p_MainLoop->_setItsProductDistribution(this);
        }
    _setItsMainLoop(p_MainLoop);
}

void ProductDistribution::cleanUpRelations() {
    if(itsMainLoop != NULL)
        {
            ProductDistribution* p_ProductDistribution = itsMainLoop->getItsProductDistribution();
            if(p_ProductDistribution != NULL)
                {
                    itsMainLoop->__setItsProductDistribution(NULL);
                }
            itsMainLoop = NULL;
        }
}

void ProductDistribution::__setItsMainLoop(MainLoop* p_MainLoop) {
    itsMainLoop = p_MainLoop;
}

void ProductDistribution::_setItsMainLoop(MainLoop* p_MainLoop) {
    if(itsMainLoop != NULL)
        {
            itsMainLoop->__setItsProductDistribution(NULL);
        }
    __setItsMainLoop(p_MainLoop);
}

void ProductDistribution::_clearItsMainLoop() {
    itsMainLoop = NULL;
}

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/ProductDistribution.cpp
*********************************************************************/
