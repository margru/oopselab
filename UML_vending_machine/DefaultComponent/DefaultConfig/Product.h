/*********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Product
//!	Generated Date	: Sat, 11, Jun 2022  
	File Path	: DefaultComponent/DefaultConfig/Product.h
*********************************************************************/

#ifndef Product_H
#define Product_H

//## auto_generated
#include <oxf/oxf.h>
//## link itsProductDist
class ProductDist;

//## package Vendigo

//## class Product
class Product {
    ////    Constructors and destructors    ////
    
public :

    //## operation Product()
    Product();
    
    //## auto_generated
    ~Product();
    
    ////    Operations    ////
    
    //## operation GetName()
    RhpString GetName();
    
    //## operation GetPrice()
    double GetPrice();
    
    //## operation GetQuant()
    int GetQuant();
    
    //## operation SetName(RhpString)
    void SetName(const RhpString& nam);
    
    //## operation SetPrice(double)
    void SetPrice(double pric);
    
    //## operation SetQuant(int)
    void SetQuant(int Quant);
    
    ////    Additional operations    ////
    
    //## auto_generated
    RhpString getName() const;
    
    //## auto_generated
    void setName(RhpString p_name);
    
    //## auto_generated
    double getPrice() const;
    
    //## auto_generated
    void setPrice(double p_price);
    
    //## auto_generated
    int getQuantity() const;
    
    //## auto_generated
    void setQuantity(int p_quantity);
    
    //## auto_generated
    ProductDist* getItsProductDist() const;
    
    //## auto_generated
    void setItsProductDist(ProductDist* p_ProductDist);
    
    //## auto_generated
    ProductDist* getItsProductDist_1() const;
    
    //## auto_generated
    void setItsProductDist_1(ProductDist* p_ProductDist);
    
    //## auto_generated
    ProductDist* getItsProductDist_2() const;
    
    //## auto_generated
    void setItsProductDist_2(ProductDist* p_ProductDist);
    
    //## auto_generated
    ProductDist* getItsProductDist_4() const;
    
    //## auto_generated
    void setItsProductDist_4(ProductDist* p_ProductDist);
    
    //## auto_generated
    ProductDist* getItsProductDist_5() const;
    
    //## auto_generated
    void setItsProductDist_5(ProductDist* p_ProductDist);
    
    //## auto_generated
    ProductDist* getItsProductDist_6() const;
    
    //## auto_generated
    void setItsProductDist_6(ProductDist* p_ProductDist);
    
    //## auto_generated
    ProductDist* getItsProductDist_7() const;
    
    //## auto_generated
    void setItsProductDist_7(ProductDist* p_ProductDist);

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Attributes    ////
    
    RhpString name;		//## attribute name
    
    double price;		//## attribute price
    
    int quantity;		//## attribute quantity
    
    ////    Relations and components    ////
    
    ProductDist* itsProductDist;		//## link itsProductDist
    
    ProductDist* itsProductDist_1;		//## link itsProductDist_1
    
    ProductDist* itsProductDist_2;		//## link itsProductDist_2
    
    ProductDist* itsProductDist_4;		//## link itsProductDist_4
    
    ProductDist* itsProductDist_5;		//## link itsProductDist_5
    
    ProductDist* itsProductDist_6;		//## link itsProductDist_6
    
    ProductDist* itsProductDist_7;		//## link itsProductDist_7
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsProductDist(ProductDist* p_ProductDist);
    
    //## auto_generated
    void _setItsProductDist(ProductDist* p_ProductDist);
    
    //## auto_generated
    void _clearItsProductDist();
    
    //## auto_generated
    void __setItsProductDist_1(ProductDist* p_ProductDist);
    
    //## auto_generated
    void _setItsProductDist_1(ProductDist* p_ProductDist);
    
    //## auto_generated
    void _clearItsProductDist_1();
    
    //## auto_generated
    void __setItsProductDist_2(ProductDist* p_ProductDist);
    
    //## auto_generated
    void _setItsProductDist_2(ProductDist* p_ProductDist);
    
    //## auto_generated
    void _clearItsProductDist_2();
    
    //## auto_generated
    void __setItsProductDist_4(ProductDist* p_ProductDist);
    
    //## auto_generated
    void _setItsProductDist_4(ProductDist* p_ProductDist);
    
    //## auto_generated
    void _clearItsProductDist_4();
    
    //## auto_generated
    void __setItsProductDist_5(ProductDist* p_ProductDist);
    
    //## auto_generated
    void _setItsProductDist_5(ProductDist* p_ProductDist);
    
    //## auto_generated
    void _clearItsProductDist_5();
    
    //## auto_generated
    void __setItsProductDist_6(ProductDist* p_ProductDist);
    
    //## auto_generated
    void _setItsProductDist_6(ProductDist* p_ProductDist);
    
    //## auto_generated
    void _clearItsProductDist_6();
    
    //## auto_generated
    void __setItsProductDist_7(ProductDist* p_ProductDist);
    
    //## auto_generated
    void _setItsProductDist_7(ProductDist* p_ProductDist);
    
    //## auto_generated
    void _clearItsProductDist_7();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Product.h
*********************************************************************/
