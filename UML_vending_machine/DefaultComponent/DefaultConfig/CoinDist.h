/*********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: CoinDist
//!	Generated Date	: Sat, 11, Jun 2022  
	File Path	: DefaultComponent/DefaultConfig/CoinDist.h
*********************************************************************/

#ifndef CoinDist_H
#define CoinDist_H

//## auto_generated
#include <oxf/oxf.h>
//## link itsMainLoop
class MainLoop;

//## package Vendigo

//## class CoinDist
class CoinDist {
    ////    Constructors and destructors    ////
    
public :

    //## operation CoinDist()
    CoinDist();
    
    //## auto_generated
    ~CoinDist();
    
    ////    Operations    ////
    
    //## operation DisplayCoins()
    void DisplayCoins();
    
    //## operation Get10()
    int Get10();
    
    //## operation Get100()
    int Get100();
    
    //## operation Get20()
    int Get20();
    
    //## operation Get200()
    int Get200();
    
    //## operation Get50()
    int Get50();
    
    //## operation Get500()
    int Get500();
    
    //## operation PayForProduct(float)
    bool PayForProduct(float pricee);
    
    ////    Additional operations    ////
    
    //## auto_generated
    int getGroszy10() const;
    
    //## auto_generated
    void setGroszy10(int p_groszy10);
    
    //## auto_generated
    int getGroszy20() const;
    
    //## auto_generated
    void setGroszy20(int p_groszy20);
    
    //## auto_generated
    int getGroszy50() const;
    
    //## auto_generated
    void setGroszy50(int p_groszy50);
    
    //## auto_generated
    int getZloty1() const;
    
    //## auto_generated
    void setZloty1(int p_zloty1);
    
    //## auto_generated
    MainLoop* getItsMainLoop() const;
    
    //## auto_generated
    void setItsMainLoop(MainLoop* p_MainLoop);

protected :

    //## auto_generated
    void cleanUpRelations();

private :

    //## auto_generated
    int getZloty2() const;
    
    //## auto_generated
    void setZloty2(int p_zloty2);
    
    //## auto_generated
    int getZloty5() const;
    
    //## auto_generated
    void setZloty5(int p_zloty5);
    
    ////    Attributes    ////

protected :

    int groszy10;		//## attribute groszy10
    
    int groszy20;		//## attribute groszy20
    
    int groszy50;		//## attribute groszy50
    
    int zloty1;		//## attribute zloty1
    
    int zloty2;		//## attribute zloty2
    
    int zloty5;		//## attribute zloty5
    
    ////    Relations and components    ////
    
    MainLoop* itsMainLoop;		//## link itsMainLoop
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsMainLoop(MainLoop* p_MainLoop);
    
    //## auto_generated
    void _setItsMainLoop(MainLoop* p_MainLoop);
    
    //## auto_generated
    void _clearItsMainLoop();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/CoinDist.h
*********************************************************************/
