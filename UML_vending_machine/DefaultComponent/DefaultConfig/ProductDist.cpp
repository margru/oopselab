/********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: ProductDist
//!	Generated Date	: Sat, 11, Jun 2022  
	File Path	: DefaultComponent/DefaultConfig/ProductDist.cpp
*********************************************************************/

//## auto_generated
#include "ProductDist.h"
//## link itsMainLoop_1
#include "MainLoop.h"
//## package Vendigo

//## class ProductDist
ProductDist::ProductDist() {
    itsMainLoop_1 = NULL;
    itsProduct_1 = NULL;
    initRelations();
    //#[ operation ProductDist()
    //itsProduct_2.name = "Zbyszko";
    std::cout<< "prod. dist. initialzied"<<std::endl;
    	//std::cout<<itsProduct_2.GetName();
    //#]
}

ProductDist::~ProductDist() {
    cleanUpRelations();
}

void ProductDist::BuyProduct(int pID) {
    //#[ operation BuyProduct(int)
    switch(pID){
    	case 1:
    	{
    	if (getItsProduct_2()->GetQuant()>0)
    		{
    			getItsProduct_2()->SetQuant(getItsProduct_2()->GetQuant()-1);
    			std::cout<< " Item bought \n";
    		}
    	else
    		{
    		std::cout<<"Item is not available! \n";
    		}
    		break;
    	}
    	case 2:
    	{
    		if (getItsProduct_4()->GetQuant()>0)
    		{
    			std::cout<< " Item bought \n";
    			getItsProduct_4()->SetQuant(getItsProduct_4()->GetQuant()-1);
    		}
    	else
    		{
    		std::cout<<"Item is not available! \n";
    		}
    		break;
    	}
    	case 3:
    	{
    		if (getItsProduct_5()->GetQuant()>0)
    		{
    			getItsProduct_5()->SetQuant(getItsProduct_5()->GetQuant()-1);
    			std::cout<< " Item bought \n";
    		}
    	else
    		{
    		std::cout<<"Item is not available! \n";
    		}
    		break;
    	}
    	case 4:
    	{
    		if (getItsProduct_6()->GetQuant()>0)
    		{
    			std::cout<< " Item bought \n";
    			getItsProduct_6()->SetQuant(getItsProduct_6()->GetQuant()-1);
    		}
    	else
    		{
    		std::cout<<"Item is not available! \n";
    		}
    		break;
    	}
    	case 5:
    	{
    		if (getItsProduct_7()->GetQuant()>0)
    		{
    			std::cout<< " Item bought \n";
    			getItsProduct_7()->SetQuant(getItsProduct_7()->GetQuant()-1);
    		}
    	else
    		{
    		std::cout<<"Item is not available! \n";
    		}
    		break;
    	}
    }
    //#]
}

void ProductDist::DisplayProducts() {
    //#[ operation DisplayProducts()
    std::cout<<"Product nr 1: " <<getItsProduct_2()->GetName() << " Price.: " << getItsProduct_2()->GetPrice() << " Quant.: " <<getItsProduct_2()->GetQuant()<<"\n";
    std::cout<<"Product nr 2: " <<getItsProduct_4()->GetName() << " Price.: " << getItsProduct_4()->GetPrice() << " Quant.: " <<getItsProduct_4()->GetQuant()<<"\n";
    std::cout<<"Product nr 3: " <<getItsProduct_5()->GetName() << " Price.: " << getItsProduct_5()->GetPrice() << " Quant.: " <<getItsProduct_5()->GetQuant()<<"\n";
    std::cout<<"Product nr 4: " <<getItsProduct_6()->GetName() << " Price.: " << getItsProduct_6()->GetPrice() << " Quant.: " <<getItsProduct_6()->GetQuant()<<"\n";
    std::cout<<"Product nr 5: " <<getItsProduct_7()->GetName() << " Price.: " << getItsProduct_7()->GetPrice() << " Quant.: " <<getItsProduct_7()->GetQuant()<<"\n";
    
    //#]
}

void ProductDist::SetTheProducts() {
    //#[ operation SetTheProducts()
    getItsProduct_4()->SetName("Woda");
    getItsProduct_4()->SetPrice(2.80);
    
    getItsProduct_2()->SetName("Zbyszko");
    getItsProduct_2()->SetPrice(4.70);
    
    getItsProduct_5()->SetName("Mleko");
    getItsProduct_5()->SetPrice(1.90);
    
    getItsProduct_6()->SetName("Wojciech");
    getItsProduct_6()->SetPrice(9.20);
    
    getItsProduct_7()->SetName("Rower");
    getItsProduct_7()->SetPrice(0.50);
    //#]
}

MainLoop* ProductDist::getItsMainLoop_1() const {
    return itsMainLoop_1;
}

void ProductDist::setItsMainLoop_1(MainLoop* p_MainLoop) {
    _setItsMainLoop_1(p_MainLoop);
}

Product* ProductDist::getItsProduct() const {
    return (Product*) &itsProduct;
}

Product* ProductDist::getItsProduct_1() const {
    return itsProduct_1;
}

void ProductDist::setItsProduct_1(Product* p_Product) {
    if(p_Product != NULL)
        {
            p_Product->_setItsProductDist_1(this);
        }
    _setItsProduct_1(p_Product);
}

Product* ProductDist::getItsProduct_2() const {
    return (Product*) &itsProduct_2;
}

Product* ProductDist::getItsProduct_4() const {
    return (Product*) &itsProduct_4;
}

Product* ProductDist::getItsProduct_5() const {
    return (Product*) &itsProduct_5;
}

Product* ProductDist::getItsProduct_6() const {
    return (Product*) &itsProduct_6;
}

Product* ProductDist::getItsProduct_7() const {
    return (Product*) &itsProduct_7;
}

void ProductDist::initRelations() {
    itsProduct._setItsProductDist(this);
    itsProduct_2._setItsProductDist_2(this);
    itsProduct_4._setItsProductDist_4(this);
    itsProduct_5._setItsProductDist_5(this);
    itsProduct_6._setItsProductDist_6(this);
    itsProduct_7._setItsProductDist_7(this);
}

void ProductDist::cleanUpRelations() {
    if(itsMainLoop_1 != NULL)
        {
            itsMainLoop_1 = NULL;
        }
    if(itsProduct_1 != NULL)
        {
            ProductDist* p_ProductDist = itsProduct_1->getItsProductDist_1();
            if(p_ProductDist != NULL)
                {
                    itsProduct_1->__setItsProductDist_1(NULL);
                }
            itsProduct_1 = NULL;
        }
}

void ProductDist::__setItsMainLoop_1(MainLoop* p_MainLoop) {
    itsMainLoop_1 = p_MainLoop;
}

void ProductDist::_setItsMainLoop_1(MainLoop* p_MainLoop) {
    __setItsMainLoop_1(p_MainLoop);
}

void ProductDist::_clearItsMainLoop_1() {
    itsMainLoop_1 = NULL;
}

void ProductDist::__setItsProduct_1(Product* p_Product) {
    itsProduct_1 = p_Product;
}

void ProductDist::_setItsProduct_1(Product* p_Product) {
    if(itsProduct_1 != NULL)
        {
            itsProduct_1->__setItsProductDist_1(NULL);
        }
    __setItsProduct_1(p_Product);
}

void ProductDist::_clearItsProduct_1() {
    itsProduct_1 = NULL;
}

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/ProductDist.cpp
*********************************************************************/
