/********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: MainLoop
//!	Generated Date	: Sat, 11, Jun 2022  
	File Path	: DefaultComponent/DefaultConfig/MainLoop.cpp
*********************************************************************/

//## auto_generated
#include "MainLoop.h"
//## package Vendigo

//## class MainLoop
MainLoop::MainLoop() {
    initRelations();
    //#[ operation MainLoop()
    std::cout<<"The MainLoop was initialized! \n";
    this->run();
    //#]
}

MainLoop::~MainLoop() {
}

void MainLoop::run() {
    //#[ operation run()
    bool cond = true;
    int pID;
    itsProductDist_1.SetTheProducts();
    while(cond)
    {
    	itsCoinDist.DisplayCoins();
    	itsProductDist_1.DisplayProducts();
    	std::cout << "Please choose product ID: \n";
    	std::cin >> pID;
    	std::cout<< " \n";
    	if(pID == 0) { cond = false; break; }
    	else if(pID >=1 && pID <= 5)
    	{
    	double price;
    	if(pID == 1) price = itsProductDist_1.getItsProduct_2()->GetPrice();
    	else if(pID == 2) price = itsProductDist_1.getItsProduct_4()->GetPrice();
    	else if(pID == 3) price = itsProductDist_1.getItsProduct_5()->GetPrice();
    	else if(pID == 4) price = itsProductDist_1.getItsProduct_6()->GetPrice();
    	else if(pID == 5) price = itsProductDist_1.getItsProduct_7()->GetPrice();
    	if(itsCoinDist.PayForProduct(price))
    		{
    			itsProductDist_1.BuyProduct(pID);
    		}
    	}
    	else std::cout<<"Wrong ID\n";
    }
    std::cout<<"Out of the loop! Bye :( \n";
    //#]
}

CoinDist* MainLoop::getItsCoinDist() const {
    return (CoinDist*) &itsCoinDist;
}

ProductDist* MainLoop::getItsProductDist_1() const {
    return (ProductDist*) &itsProductDist_1;
}

void MainLoop::initRelations() {
    itsCoinDist._setItsMainLoop(this);
    itsProductDist_1._setItsMainLoop_1(this);
}

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/MainLoop.cpp
*********************************************************************/
