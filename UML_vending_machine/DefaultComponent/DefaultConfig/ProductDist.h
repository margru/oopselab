/*********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: ProductDist
//!	Generated Date	: Sat, 11, Jun 2022  
	File Path	: DefaultComponent/DefaultConfig/ProductDist.h
*********************************************************************/

#ifndef ProductDist_H
#define ProductDist_H

//## auto_generated
#include <oxf/oxf.h>
//## link itsProduct
#include "Product.h"
//## link itsMainLoop_1
class MainLoop;

//## package Vendigo

//## class ProductDist
class ProductDist {
    ////    Constructors and destructors    ////
    
public :

    //## operation ProductDist()
    ProductDist();
    
    //## auto_generated
    ~ProductDist();
    
    ////    Operations    ////
    
    //## operation BuyProduct(int)
    void BuyProduct(int pID);
    
    //## operation DisplayProducts()
    void DisplayProducts();
    
    //## operation SetTheProducts()
    void SetTheProducts();
    
    ////    Additional operations    ////
    
    //## auto_generated
    MainLoop* getItsMainLoop_1() const;
    
    //## auto_generated
    void setItsMainLoop_1(MainLoop* p_MainLoop);
    
    //## auto_generated
    Product* getItsProduct() const;
    
    //## auto_generated
    Product* getItsProduct_1() const;
    
    //## auto_generated
    void setItsProduct_1(Product* p_Product);
    
    //## auto_generated
    Product* getItsProduct_2() const;
    
    //## auto_generated
    Product* getItsProduct_4() const;
    
    //## auto_generated
    Product* getItsProduct_5() const;
    
    //## auto_generated
    Product* getItsProduct_6() const;
    
    //## auto_generated
    Product* getItsProduct_7() const;

protected :

    //## auto_generated
    void initRelations();
    
    //## auto_generated
    void cleanUpRelations();
    
    ////    Relations and components    ////
    
    MainLoop* itsMainLoop_1;		//## link itsMainLoop_1
    
    Product itsProduct;		//## link itsProduct
    
    Product* itsProduct_1;		//## link itsProduct_1
    
    Product itsProduct_2;		//## link itsProduct_2
    
    Product itsProduct_4;		//## link itsProduct_4
    
    Product itsProduct_5;		//## link itsProduct_5
    
    Product itsProduct_6;		//## link itsProduct_6
    
    Product itsProduct_7;		//## link itsProduct_7
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsMainLoop_1(MainLoop* p_MainLoop);
    
    //## auto_generated
    void _setItsMainLoop_1(MainLoop* p_MainLoop);
    
    //## auto_generated
    void _clearItsMainLoop_1();
    
    //## auto_generated
    void __setItsProduct_1(Product* p_Product);
    
    //## auto_generated
    void _setItsProduct_1(Product* p_Product);
    
    //## auto_generated
    void _clearItsProduct_1();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/ProductDist.h
*********************************************************************/
