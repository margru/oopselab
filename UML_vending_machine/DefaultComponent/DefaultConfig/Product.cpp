/********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Product
//!	Generated Date	: Sat, 11, Jun 2022  
	File Path	: DefaultComponent/DefaultConfig/Product.cpp
*********************************************************************/

//## auto_generated
#include "Product.h"
//## link itsProductDist
#include "ProductDist.h"
//## package Vendigo

//## class Product
Product::Product() : quantity(10) {
    itsProductDist = NULL;
    itsProductDist_1 = NULL;
    itsProductDist_2 = NULL;
    itsProductDist_4 = NULL;
    itsProductDist_5 = NULL;
    itsProductDist_6 = NULL;
    itsProductDist_7 = NULL;
    //#[ operation Product()
    std::cout<<"Product "<< this->name <<" initialized! \n";
    //#]
}

Product::~Product() {
    cleanUpRelations();
}

RhpString Product::GetName() {
    //#[ operation GetName()
    return this->name;
    //#]
}

double Product::GetPrice() {
    //#[ operation GetPrice()
    return price;
    //#]
}

int Product::GetQuant() {
    //#[ operation GetQuant()
    return quantity;
    //#]
}

void Product::SetName(const RhpString& nam) {
    //#[ operation SetName(RhpString)
    name = nam;
    //#]
}

void Product::SetPrice(double pric) {
    //#[ operation SetPrice(double)
    price = pric;
    //#]
}

void Product::SetQuant(int Quant) {
    //#[ operation SetQuant(int)
    quantity = Quant;
    //#]
}

RhpString Product::getName() const {
    return name;
}

void Product::setName(RhpString p_name) {
    name = p_name;
}

double Product::getPrice() const {
    return price;
}

void Product::setPrice(double p_price) {
    price = p_price;
}

int Product::getQuantity() const {
    return quantity;
}

void Product::setQuantity(int p_quantity) {
    quantity = p_quantity;
}

ProductDist* Product::getItsProductDist() const {
    return itsProductDist;
}

void Product::setItsProductDist(ProductDist* p_ProductDist) {
    _setItsProductDist(p_ProductDist);
}

ProductDist* Product::getItsProductDist_1() const {
    return itsProductDist_1;
}

void Product::setItsProductDist_1(ProductDist* p_ProductDist) {
    if(p_ProductDist != NULL)
        {
            p_ProductDist->_setItsProduct_1(this);
        }
    _setItsProductDist_1(p_ProductDist);
}

ProductDist* Product::getItsProductDist_2() const {
    return itsProductDist_2;
}

void Product::setItsProductDist_2(ProductDist* p_ProductDist) {
    _setItsProductDist_2(p_ProductDist);
}

ProductDist* Product::getItsProductDist_4() const {
    return itsProductDist_4;
}

void Product::setItsProductDist_4(ProductDist* p_ProductDist) {
    _setItsProductDist_4(p_ProductDist);
}

ProductDist* Product::getItsProductDist_5() const {
    return itsProductDist_5;
}

void Product::setItsProductDist_5(ProductDist* p_ProductDist) {
    _setItsProductDist_5(p_ProductDist);
}

ProductDist* Product::getItsProductDist_6() const {
    return itsProductDist_6;
}

void Product::setItsProductDist_6(ProductDist* p_ProductDist) {
    _setItsProductDist_6(p_ProductDist);
}

ProductDist* Product::getItsProductDist_7() const {
    return itsProductDist_7;
}

void Product::setItsProductDist_7(ProductDist* p_ProductDist) {
    _setItsProductDist_7(p_ProductDist);
}

void Product::cleanUpRelations() {
    if(itsProductDist != NULL)
        {
            itsProductDist = NULL;
        }
    if(itsProductDist_1 != NULL)
        {
            Product* p_Product = itsProductDist_1->getItsProduct_1();
            if(p_Product != NULL)
                {
                    itsProductDist_1->__setItsProduct_1(NULL);
                }
            itsProductDist_1 = NULL;
        }
    if(itsProductDist_2 != NULL)
        {
            itsProductDist_2 = NULL;
        }
    if(itsProductDist_4 != NULL)
        {
            itsProductDist_4 = NULL;
        }
    if(itsProductDist_5 != NULL)
        {
            itsProductDist_5 = NULL;
        }
    if(itsProductDist_6 != NULL)
        {
            itsProductDist_6 = NULL;
        }
    if(itsProductDist_7 != NULL)
        {
            itsProductDist_7 = NULL;
        }
}

void Product::__setItsProductDist(ProductDist* p_ProductDist) {
    itsProductDist = p_ProductDist;
}

void Product::_setItsProductDist(ProductDist* p_ProductDist) {
    __setItsProductDist(p_ProductDist);
}

void Product::_clearItsProductDist() {
    itsProductDist = NULL;
}

void Product::__setItsProductDist_1(ProductDist* p_ProductDist) {
    itsProductDist_1 = p_ProductDist;
}

void Product::_setItsProductDist_1(ProductDist* p_ProductDist) {
    if(itsProductDist_1 != NULL)
        {
            itsProductDist_1->__setItsProduct_1(NULL);
        }
    __setItsProductDist_1(p_ProductDist);
}

void Product::_clearItsProductDist_1() {
    itsProductDist_1 = NULL;
}

void Product::__setItsProductDist_2(ProductDist* p_ProductDist) {
    itsProductDist_2 = p_ProductDist;
}

void Product::_setItsProductDist_2(ProductDist* p_ProductDist) {
    __setItsProductDist_2(p_ProductDist);
}

void Product::_clearItsProductDist_2() {
    itsProductDist_2 = NULL;
}

void Product::__setItsProductDist_4(ProductDist* p_ProductDist) {
    itsProductDist_4 = p_ProductDist;
}

void Product::_setItsProductDist_4(ProductDist* p_ProductDist) {
    __setItsProductDist_4(p_ProductDist);
}

void Product::_clearItsProductDist_4() {
    itsProductDist_4 = NULL;
}

void Product::__setItsProductDist_5(ProductDist* p_ProductDist) {
    itsProductDist_5 = p_ProductDist;
}

void Product::_setItsProductDist_5(ProductDist* p_ProductDist) {
    __setItsProductDist_5(p_ProductDist);
}

void Product::_clearItsProductDist_5() {
    itsProductDist_5 = NULL;
}

void Product::__setItsProductDist_6(ProductDist* p_ProductDist) {
    itsProductDist_6 = p_ProductDist;
}

void Product::_setItsProductDist_6(ProductDist* p_ProductDist) {
    __setItsProductDist_6(p_ProductDist);
}

void Product::_clearItsProductDist_6() {
    itsProductDist_6 = NULL;
}

void Product::__setItsProductDist_7(ProductDist* p_ProductDist) {
    itsProductDist_7 = p_ProductDist;
}

void Product::_setItsProductDist_7(ProductDist* p_ProductDist) {
    __setItsProductDist_7(p_ProductDist);
}

void Product::_clearItsProductDist_7() {
    itsProductDist_7 = NULL;
}

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Product.cpp
*********************************************************************/
