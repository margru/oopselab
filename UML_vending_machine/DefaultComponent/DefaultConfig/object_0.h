/*********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: object_0
//!	Generated Date	: Sat, 11, Jun 2022  
	File Path	: DefaultComponent/DefaultConfig/object_0.h
*********************************************************************/

#ifndef object_0_H
#define object_0_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include "Vendigo.h"
//## package Vendigo

//## class TopLevel::object_0
class object_0_C {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    object_0_C();
    
    //## auto_generated
    ~object_0_C();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/object_0.h
*********************************************************************/
