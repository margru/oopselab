/********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: produkciki
//!	Generated Date	: Sat, 11, Jun 2022  
	File Path	: DefaultComponent/DefaultConfig/produkciki.cpp
*********************************************************************/

//## auto_generated
#include "produkciki.h"
//## package Vendigo

//## class produkciki
produkciki::produkciki() {
}

produkciki::~produkciki() {
}

Product* produkciki::getWoda() const {
    return (Product*) &Woda;
}

Product* produkciki::getZbyszko3Cytryny() const {
    return (Product*) &Zbyszko3Cytryny;
}

Product* produkciki::getP1() const {
    return (Product*) &p1;
}

Product* produkciki::getP2() const {
    return (Product*) &p2;
}

Product* produkciki::getP3() const {
    return (Product*) &p3;
}

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/produkciki.cpp
*********************************************************************/
