/*********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: ProductDistribution
//!	Generated Date	: Sat, 11, Jun 2022  
	File Path	: DefaultComponent/DefaultConfig/ProductDistribution.h
*********************************************************************/

#ifndef ProductDistribution_H
#define ProductDistribution_H

//## auto_generated
#include <oxf/oxf.h>
//## link itsMainLoop
class MainLoop;

//## package Vendigo

//## class ProductDistribution
class ProductDistribution {
    ////    Constructors and destructors    ////
    
public :

    //## operation ProductDistribution()
    ProductDistribution();
    
    //## auto_generated
    ~ProductDistribution();
    
    ////    Additional operations    ////
    
    //## auto_generated
    MainLoop* getItsMainLoop() const;
    
    //## auto_generated
    void setItsMainLoop(MainLoop* p_MainLoop);

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Relations and components    ////
    
    MainLoop* itsMainLoop;		//## link itsMainLoop
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsMainLoop(MainLoop* p_MainLoop);
    
    //## auto_generated
    void _setItsMainLoop(MainLoop* p_MainLoop);
    
    //## auto_generated
    void _clearItsMainLoop();
};

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/ProductDistribution.h
*********************************************************************/
