/*********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: produkciki
//!	Generated Date	: Sat, 11, Jun 2022  
	File Path	: DefaultComponent/DefaultConfig/produkciki.h
*********************************************************************/

#ifndef produkciki_H
#define produkciki_H

//## auto_generated
#include <oxf/oxf.h>
//## classInstance Woda
#include "Product.h"
//## package Vendigo

//## class produkciki
class produkciki {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    produkciki();
    
    //## auto_generated
    ~produkciki();
    
    ////    Additional operations    ////
    
    //## auto_generated
    Product* getWoda() const;
    
    //## auto_generated
    Product* getZbyszko3Cytryny() const;
    
    //## auto_generated
    Product* getP1() const;
    
    //## auto_generated
    Product* getP2() const;
    
    //## auto_generated
    Product* getP3() const;
    
    ////    Relations and components    ////

protected :

    Product Woda;		//## classInstance Woda
    
    Product Zbyszko3Cytryny;		//## classInstance Zbyszko3Cytryny
    
    Product p1;		//## classInstance p1
    
    Product p2;		//## classInstance p2
    
    Product p3;		//## classInstance p3
};

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/produkciki.h
*********************************************************************/
