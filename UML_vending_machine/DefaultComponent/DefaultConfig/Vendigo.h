/*********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Vendigo
//!	Generated Date	: Sat, 11, Jun 2022  
	File Path	: DefaultComponent/DefaultConfig/Vendigo.h
*********************************************************************/

#ifndef Vendigo_H
#define Vendigo_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
class MainLoop;

//## auto_generated
class ProductDistribution;

//## classInstance object_0
class object_0_C;

//## package Vendigo


//## classInstance object_0
extern object_0_C object_0;

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Vendigo.h
*********************************************************************/
