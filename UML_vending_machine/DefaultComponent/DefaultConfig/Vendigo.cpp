/********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Vendigo
//!	Generated Date	: Sat, 11, Jun 2022  
	File Path	: DefaultComponent/DefaultConfig/Vendigo.cpp
*********************************************************************/

//## auto_generated
#include "Vendigo.h"
//## classInstance object_0
#include "object_0.h"
//## auto_generated
#include "MainLoop.h"
//## auto_generated
#include "ProductDistribution.h"
//## package Vendigo


//## classInstance object_0
object_0_C object_0;

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Vendigo.cpp
*********************************************************************/
